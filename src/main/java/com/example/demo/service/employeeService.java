package com.example.demo.service;

import com.example.demo.entity.employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.repository.employeeRepository;

import java.util.List;

@Service
public class employeeService {
    @Autowired
private employeeRepository repo;

    public List<employee> getAll(){
        return repo.findAll();
    }
}
