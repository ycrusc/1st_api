package com.example.demo.controller;

import org.springframework.web.bind.annotation.*;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
public class cobacontroller {
    @GetMapping("/v1/messages")
    public String welcome() {
        return "Welcome to Springboot Rest API";
    }

 @PostMapping("/v1/hola/{name}")
 @ResponseBody
 public static Map<String, Object> defaultController(@PathVariable String name) {
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("message", "Hello" + name);
        return map;
    }

@GetMapping("/v1/hola/")
@ResponseBody
public static Map<String, Object> defaultController(){
    Map<String, Object> map = new LinkedHashMap<>();
    map.put("message", "Hello" );
    return map;
}
}