package com.example.demo.entity;

import com.example.demo.service.employeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class employeeController {
    @Autowired
    private employeeService service;

    @GetMapping("/employee")
    public Iterable<employee> employeeList(){
        return service.getAll();
    }
}
